package edu.fsu.cs.mobile.geofenceapp;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import java.util.ArrayList;
public class HistoryActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_history);

        ListView history = findViewById(R.id.history);

        ArrayList<String> recordedTimes = getIntent().getStringArrayListExtra("array");

        ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1,recordedTimes);
        history.setAdapter(adapter);
    }
}

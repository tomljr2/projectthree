package edu.fsu.cs.mobile.geofenceapp;

import android.Manifest;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.SystemClock;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Chronometer;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class MainActivity extends AppCompatActivity implements LocationListener,OnMapReadyCallback {

    Location geofenceLoc;
    static float size;
    private GoogleMap map;
    Marker marker;
    Circle geoFence;
    float lat,lon;
    boolean start,stopped;
    Chronometer timer;
    long base = SystemClock.elapsedRealtime();
    static ArrayList<String> recordedTimes;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);



        final TextView geoSize = findViewById(R.id.geoSize);
        SeekBar setGeofenceSize = findViewById(R.id.geoScroll);
        timer = findViewById(R.id.timer);
        start = true;
        stopped = false;

        SharedPreferences savedLoc = getSharedPreferences("geoFence",0);
        lat = (savedLoc.getFloat("geoLat",0));
        lon = (savedLoc.getFloat("geoLong",0));
        size = (savedLoc.getFloat("geoSize",1));
        try {
            recordedTimes = (ArrayList<String>) ObjectSerializer.deserialize(savedLoc.getString("ltime",ObjectSerializer.serialize(new ArrayList<String>())));
        } catch (IOException e) {
            e.printStackTrace();
        }

        timer.setBase(base);//savedLoc.getLong("geoTime",SystemClock.elapsedRealtime()));
        geoSize.setText("" + (int)size + " m");

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        if(geofenceLoc != null)
            geoFence = map.addCircle(new CircleOptions().center(new LatLng(geofenceLoc.getLatitude(),geofenceLoc.getLongitude()))
                    .radius((int)size).fillColor(0x220000FF).strokeColor(Color.BLUE));

        //These few lines of code set up the GPS and track the location of the user
        Criteria criteria = new Criteria();
        criteria.setAccuracy(Criteria.ACCURACY_FINE);
        final LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        locationManager.getBestProvider(criteria, true);
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(MainActivity.this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
            return;
        }
        locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);

        //This will update the text to show the range of the geofence
        setGeofenceSize.setMax(99);
        setGeofenceSize.setProgress((int)size);
        setGeofenceSize.refreshDrawableState();
        setGeofenceSize.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                i+=1;
                size = (float) i;
                if(geoFence != null && geofenceLoc != null)
                {
                    geoFence.remove();
                    geoFence = map.addCircle(new CircleOptions().center(new LatLng(geofenceLoc.getLatitude(),geofenceLoc.getLongitude()))
                            .radius((int)size).fillColor(0x220000FF).strokeColor(Color.BLUE));
                }

                geoSize.setText(i + " m");
            }
            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {}
            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {}
        });
    }

    @Override
    public void onLocationChanged(final Location location) {
        Button setGeofence = findViewById(R.id.setGeoButton);

        if(start)
        {
            timer.setBase(SystemClock.elapsedRealtime());
        }
        if(geofenceLoc == null && start)
        {
            geofenceLoc = location;
            geofenceLoc.setLatitude(lat);
            geofenceLoc.setLongitude(lon);
        }
        if(geofenceLoc != null && geofenceLoc.getLatitude() != 0 && geofenceLoc.getLongitude() != 0 && start)
        {
            geoFence = map.addCircle(new CircleOptions().center(new LatLng(geofenceLoc.getLatitude(),geofenceLoc.getLongitude()))
                    .radius((int)size).fillColor(0x220000FF).strokeColor(Color.BLUE));
            start = false;
        }
        if(geofenceLoc != null && geofenceLoc.getLatitude() == 0 && geofenceLoc.getLongitude() == 0 && start)
        {
            geofenceLoc = null;
            start = false;
        }

        marker.remove();
        marker = map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .position(new LatLng(location.getLatitude(),location.getLongitude())));
        map.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(location.getLatitude(),location.getLongitude())));


        //This will let the user know if they are in the geofence that they set
        if(geofenceLoc != null && location.distanceTo(geofenceLoc) <= size)
        {
            if(stopped)
            {
                //SharedPreferences savedLoc = getSharedPreferences("geoFence",0);
                timer.setBase(timer.getBase() + SystemClock.elapsedRealtime() - base);//savedLoc.getLong("geoTime",SystemClock.elapsedRealtime()));
            }
            timer.start();
            stopped = false;
        }
        else if(geofenceLoc != null)
        {
            //inGeofence.setText("You are not in the geofence");
            if(!stopped)
            {
               // SharedPreferences savedLoc = getSharedPreferences("geoFence",0);
                //SharedPreferences.Editor editor = savedLoc.edit();
                //editor.putLong("geoTime",SystemClock.elapsedRealtime());
                //editor.commit();
                base = SystemClock.elapsedRealtime();
            }
            stopped = true;
            timer.stop();

        }

        //Pressing the button will set the geofence
        setGeofence.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                SharedPreferences savedLoc = getSharedPreferences("geoFence",0);
                SharedPreferences.Editor editor = savedLoc.edit();
                timer.setBase(SystemClock.elapsedRealtime());

                geofenceLoc = location;
                if(geoFence != null)
                    geoFence.remove();
                geoFence = map.addCircle(new CircleOptions().center(new LatLng(geofenceLoc.getLatitude(),geofenceLoc.getLongitude()))
                    .radius((int)size).fillColor(0x220000FF).strokeColor(Color.BLUE));

                editor.putFloat("geoLat",(float)geofenceLoc.getLatitude());
                editor.putFloat("geoLong",(float)geofenceLoc.getLongitude());
                editor.putFloat("geoSize",size);
                editor.commit();
            }
        });
    }

    @Override
    public void onProviderEnabled(String s) {}
    @Override
    public void onProviderDisabled(String s) {
        //If GPS is disabled mid-app, then tell the user to turn it on
        Toast.makeText(MainActivity.this, "Please Enable GPS", Toast.LENGTH_SHORT).show();
    }
    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case R.id.removeGeo:
                if(geoFence != null)
                    geoFence.remove();
                SharedPreferences savedLoc = getSharedPreferences("geoFence",0);
                SharedPreferences.Editor editor = savedLoc.edit();
                if(geofenceLoc != null)
                {
                    Calendar c = Calendar.getInstance();
                    recordedTimes.add(c.get(Calendar.YEAR) + "-" + c.get(Calendar.MONTH) + "-"
                            + c.get(Calendar.DAY_OF_MONTH) + "\t" + "Time Elapsed: " + timer.getText().toString());
                    try {
                        editor.putString("ltime", ObjectSerializer.serialize(recordedTimes));

                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                editor.putFloat("geoLat",0);
                editor.putFloat("geoLong", 0);
                editor.commit();
                geofenceLoc = null;
                timer.stop();
                stopped = false;



                break;
            case R.id.ViewHistory:
                Intent intent = new Intent(this, HistoryActivity.class);
                intent.putExtra("array", recordedTimes);
                startActivity(intent);
                break;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.moveCamera(CameraUpdateFactory.newLatLng(new LatLng(30.4383,-84.2807)));
        googleMap.moveCamera(CameraUpdateFactory.zoomTo(18));
        map = googleMap;
        marker = map.addMarker(new MarkerOptions().icon(BitmapDescriptorFactory.fromResource(R.drawable.marker))
                .position(new LatLng(30.4383,-84.2807)));
        map.moveCamera(CameraUpdateFactory.zoomTo(20));
    }
}
